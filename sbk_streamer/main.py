import datetime
import os
import urllib

from fastapi import FastAPI, HTTPException
from starlette.responses import StreamingResponse
import urllib.request

from sbk_streamer.token_codec import decode

app = FastAPI()


def key_is_valid(key: str) -> bool:
    start, stop = decode(key)
    now = datetime.datetime.now()
    in_range = start < now < stop
    delta = stop - start
    delta_is_ok = delta.total_seconds() < 60*60*24
    return in_range and delta_is_ok


@app.get("/stream/{format}/{filename}")
async def root(format: str, filename: str, key: str = ''):
    # .ts файлы имеют динамические имена, их невозможно подобрать
    if not filename.endswith('.ts'):
        if not key_is_valid(key):
            raise HTTPException(status_code=404)

    def iter_hls_file():
        host = os.getenv('SBK_SOURCE', 'http://127.0.0.1')
        url = f'{host}/{format}/{filename}'
        for chunk in urllib.request.urlopen(url):
            yield chunk

    return StreamingResponse(iter_hls_file(), media_type="video/mp4")

