import datetime
from typing import Tuple


def _numbers(sdt: datetime):
    return (sdt.year, sdt.month, sdt.day, sdt.hour, sdt.minute)


def _shift(number_position):
    now = datetime.datetime.now()
    new_year = datetime.datetime(now.year, 1, 1)
    day_count = (now - new_year).days
    return day_count + 2 * number_position


def _encode_number(number: int, number_position: int) -> str:
    shifted_number = number + _shift(number_position)
    return f'{shifted_number:03x}'


def _decode_number(hex_number: str, number_position: int) -> int:
    int_number = int(hex_number, 16)
    return int_number - _shift(number_position)


def _encode_numbers(numbers, start_index):
    encoded_numbers = [
        _encode_number(number, start_index + index)
        for index, number in enumerate(numbers)
    ]
    return ''.join(encoded_numbers)


def _decode_numbers(hex_numbers, start_index):
    decoded_numbers = [
        _decode_number(hex_number, start_index + index)
        for index, hex_number in enumerate(hex_numbers)
    ]
    return decoded_numbers


def _encode_datetime(dt: datetime.datetime, start_index: int) -> str:
    dt_numbers = _numbers(dt)
    return _encode_numbers(dt_numbers, start_index)


def _decode_datetime(dt_str, start_index):
    hex_numbers = [
        dt_str[3 * idx: 3 * idx + 3] for idx in range(5)
    ]
    numbers = _decode_numbers(hex_numbers, start_index)
    return datetime.datetime(*numbers)


def encode(from_time: datetime.datetime, to_time: datetime.datetime) -> str:
    enc_from = _encode_datetime(from_time, 0)
    enc_to = _encode_datetime(to_time, 5)

    return f'{enc_from}{enc_to}'


def decode(encoded: str) -> Tuple[datetime.datetime, datetime.datetime]:
    from_str = encoded[:15]
    to_str = encoded[15:]

    return _decode_datetime(from_str, 0), _decode_datetime(to_str, 5)
