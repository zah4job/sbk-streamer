import datetime

import pytest

from sbk_streamer.token_codec import encode, decode


def test_encode_decode():
    src = datetime.datetime(2024, 2, 8, 22, 7)
    now = datetime.datetime(src.year, src.month, src.day, src.hour, src.minute)
    later = now + datetime.timedelta(minutes=183)

    encoded = encode(now, later)
    res_now, res_later = decode(encoded)

    assert res_now == now
    assert res_later == later
